package az.ingress.turboms24;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Turboms24Application {

	public static void main(String[] args) {
		SpringApplication.run(Turboms24Application.class, args);
	}

}
