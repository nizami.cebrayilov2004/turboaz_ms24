package az.ingress.turboms24.service;

import az.ingress.turboms24.dto.ProductAnalyticsRequestDto;
import az.ingress.turboms24.dto.ProductAnalyticsResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductAnalyticsService {


    ProductAnalyticsResponseDto createProductAnalytics(ProductAnalyticsRequestDto productAnalyticsRequestDto);

    ProductAnalyticsResponseDto getProductAnalyticsById(Long id);

    ProductAnalyticsResponseDto updateProductAnalytics(Long id, ProductAnalyticsRequestDto productAnalyticsRequestDto);

    void deleteProductAnalyticsById(Long id);

    Page<ProductAnalyticsResponseDto> productAnalyticsList(Pageable page);
}
