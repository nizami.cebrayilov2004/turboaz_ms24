package az.ingress.turboms24.service;

import az.ingress.turboms24.domain.ModelEntity;
import az.ingress.turboms24.dto.ModelRequestDto;
import az.ingress.turboms24.dto.ModelResponseDto;
import az.ingress.turboms24.repo.ModelRepository;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

@Service
@RequiredArgsConstructor
public class ModelServiceImpl implements ModelService {
    private final ModelRepository modelRepository;
    @Override
    public ModelResponseDto createModel(ModelRequestDto modelRequestDto) {
        ModelEntity modelEntity =
                ModelEntity.builder()
                        .name(modelRequestDto.getName())
                        .build();
        final ModelEntity modelEntitySaved = modelRepository.save(modelEntity);
        return ModelResponseDto.builder()
                .name(modelEntitySaved.getName())
                .build();
    }

    @Override
    public ModelResponseDto getModelById(Long id) {
        final ModelEntity modelEntity = modelRepository.findById(id)
                .orElseThrow();
        return ModelResponseDto.builder()
                .name(modelEntity.getName())
                .build();
    }

    @Override
    public ModelResponseDto updateModel(Long id, ModelRequestDto modelRequestDto) {
        ModelEntity modelEntity = ModelEntity.builder()
                .id(id)
                .name(modelRequestDto.getName())
                .build();
        final ModelEntity modelEntitySaved = modelRepository.save(modelEntity);
        return ModelResponseDto.builder()
                .name(modelEntitySaved.getName())
                .build();
    }

    @Override
    public void deleteModelById(Long id) {
modelRepository.deleteById(id);
    }

    @Override
    public Page<ModelResponseDto> modelList(@PageableDefault(size = 3, page = 1) Pageable pageable) {
        return modelRepository.findAll(pageable)
                .map(modelEntity -> ModelResponseDto.builder()
                        .name(modelEntity.getName())
                        .build());
    }
}
