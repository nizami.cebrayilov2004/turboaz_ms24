package az.ingress.turboms24.service;

import az.ingress.turboms24.dto.ModelRequestDto;
import az.ingress.turboms24.dto.ModelResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ModelService {


    ModelResponseDto createModel(ModelRequestDto modelRequestDto);

    ModelResponseDto getModelById(Long id);

    ModelResponseDto updateModel(Long id, ModelRequestDto modelRequestDto);

    void deleteModelById(Long id);

    Page<ModelResponseDto> modelList(Pageable page);
}