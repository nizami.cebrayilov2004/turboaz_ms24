package az.ingress.turboms24.repo;

import az.ingress.turboms24.domain.ModelEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ModelRepository extends JpaRepository<ModelEntity, Long> {
}
