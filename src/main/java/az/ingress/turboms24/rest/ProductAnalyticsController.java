package az.ingress.turboms24.rest;
import az.ingress.turboms24.dto.ProductAnalyticsRequestDto;
import az.ingress.turboms24.dto.ProductAnalyticsResponseDto;
import az.ingress.turboms24.service.ProductAnalyticsService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product-analytics")
@RequiredArgsConstructor
public class ProductAnalyticsController {

    private final ProductAnalyticsService productAnalyticsService;

    @GetMapping("/{id}")
    public ProductAnalyticsResponseDto getProductAnalyticsById(@PathVariable Long id) {
        return productAnalyticsService.getProductAnalyticsById(id);
    }

    @PostMapping()
    public ProductAnalyticsResponseDto createProductAnalytics(@RequestBody @Validated ProductAnalyticsRequestDto productAnalyticsRequestDto) {
        return productAnalyticsService.createProductAnalytics(productAnalyticsRequestDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProductAnalyticsById(@PathVariable Long id){
        productAnalyticsService.deleteProductAnalyticsById(id);
    }

    @PutMapping("/{id}")
    public ProductAnalyticsResponseDto update
            (@PathVariable Long id,
             @RequestBody ProductAnalyticsRequestDto productAnalyticsRequestDto){
        return productAnalyticsService.updateProductAnalytics(id, productAnalyticsRequestDto);
    }

    @GetMapping
    public Page<ProductAnalyticsResponseDto> getProductAnalyticsList(Pageable pageable){
        return productAnalyticsService.productAnalyticsList(pageable);
    }




}
