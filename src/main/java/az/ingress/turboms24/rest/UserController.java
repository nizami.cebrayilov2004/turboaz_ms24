package az.ingress.turboms24.rest;

import az.ingress.turboms24.dto.UserRequestDto;
import az.ingress.turboms24.dto.UserResponseDto;
import az.ingress.turboms24.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping("/{id}")
    public UserResponseDto getUserById(@PathVariable Long id){
        return userService.getUserById(id);
    }

    @PostMapping()
    public UserResponseDto createUser(@RequestBody @Validated UserRequestDto userRequestDto){
        return userService.createUser(userRequestDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUserById(@PathVariable Long id){
        userService.deleteUserById(id);
    }

    @PutMapping("/{id}")
    public UserResponseDto updateUser(@PathVariable Long id, @RequestBody UserRequestDto userRequestDto){
     return userService.updateUser(id, userRequestDto);
    }

    @GetMapping
    public Page<UserResponseDto> getUserList(Pageable pageable){
        return userService.userList(pageable);
    }
}
