package az.ingress.turboms24.service;

import az.ingress.turboms24.domain.ProductEntity;
import az.ingress.turboms24.dto.ProductRequestDto;
import az.ingress.turboms24.dto.ProductResponseDto;
import az.ingress.turboms24.repo.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;

    @Override
    public ProductResponseDto createProduct(ProductRequestDto requestDto) {

        ProductEntity productEntity =
                ProductEntity.builder()
                        //   .productAnalytics(requestDto.getProductAnalytics())
                        .carAdId(requestDto.getCarAdId())
                        .productDetail(requestDto.getProductDetail())
                        .price(requestDto.getPrice())
                        .priceCurrency(requestDto.getPriceCurrency())
                        .build();
        final ProductEntity productSaved = productRepository.save(productEntity);
        return ProductResponseDto.builder()
                .id(productSaved.getId())
                //     .productAnalytics(productSaved.getProductAnalytics())
                .carAdId(productSaved.getCarAdId())
                .productDetail(productSaved.getProductDetail())
                .price(productSaved.getPrice())
                .priceCurrency(productSaved.getPriceCurrency())
                .user(productSaved.getUser())
                .build();


    }

    @Override
    public ProductResponseDto getProductById(Long id) {

        final ProductEntity product = productRepository.findById(id)
                .orElseThrow();

        return ProductResponseDto.builder()
                .id(product.getId())
                .name(product.getName())
                .price(product.getPrice())
                .priceCurrency(product.getPriceCurrency())
                .productDetail(product.getProductDetail())
                .carAdId(product.getCarAdId())
                .user(product.getUser())
                .build();
    }

    @Override
    public ProductResponseDto updateProduct(Long id, ProductRequestDto productRequestDto) {
        ProductEntity productEntity =
                ProductEntity
                        .builder()
                        .id(id)
//                        .productAnalytics(productRequestDto.getProductAnalytics())
                        .carAdId(productRequestDto.getCarAdId())
                        .productDetail(productRequestDto.getProductDetail())
                        .price(productRequestDto.getPrice())
                        .priceCurrency(productRequestDto.getPriceCurrency())
                        .user(productRequestDto.getUser())
                        .name(productRequestDto.getName())
                        .build();
        final ProductEntity productSaved = productRepository.save(productEntity);
        return ProductResponseDto
                .builder()
                .id(productSaved.getId())
//                .productAnalytics(productSaved.getProductAnalytics())
                .carAdId(productSaved.getCarAdId())
                .productDetail(productSaved.getProductDetail())
                .price(productSaved.getPrice())
                .priceCurrency(productSaved.getPriceCurrency())
                .name(productSaved.getName())
                .user(productSaved.getUser())
                .build();

    }

    @Override
    public void deleteProductById(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public Page<ProductResponseDto> productList(@PageableDefault(size = 7, page = 1) Pageable pageable){
        return productRepository.findAll(pageable)
                .map(productEntity -> ProductResponseDto
                        .builder()
                        .id(productEntity.getId())
                        .user(productEntity.getUser())
                        .priceCurrency(productEntity.getPriceCurrency())
                        .carAdId(productEntity.getCarAdId())
                        .productDetail(productEntity.getProductDetail())
                        .price(productEntity.getPrice())
                        .name(productEntity.getName())
//                        .productAnalytics(productEntity.getProductAnalytics())
                        .build());
    }
}
