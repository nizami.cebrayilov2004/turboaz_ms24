package az.ingress.turboms24.service;

import az.ingress.turboms24.dto.UserRequestDto;
import az.ingress.turboms24.dto.UserResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {


    UserResponseDto createUser(UserRequestDto userRequestDto);

    UserResponseDto getUserById(Long id);

    UserResponseDto updateUser(Long id, UserRequestDto userRequestDto);

    void deleteUserById(Long id);

    Page<UserResponseDto> userList(Pageable page);
}

