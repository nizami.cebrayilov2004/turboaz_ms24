package az.ingress.turboms24.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ModelRequestDto {
    private Long id;
    private String name;

}
