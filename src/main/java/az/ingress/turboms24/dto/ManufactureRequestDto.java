package az.ingress.turboms24.dto;

import az.ingress.turboms24.domain.ModelEntity;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ManufactureRequestDto {
    private Long id;
    private String name;
    private List<ModelEntity> models;
}
