package az.ingress.turboms24.domain;
import jakarta.persistence.*;
import lombok.*;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder

public class ProductEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(cascade = CascadeType.PERSIST)
    private UserEntity user;
    private String name;
    private Double price;
    private String priceCurrency;
    private String productDetail;
    private Long carAdId;
    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private ProductAnalyticsEntity productAnalytics;
}
