package az.ingress.turboms24.dto;

import az.ingress.turboms24.domain.ProductAnalyticsEntity;
import az.ingress.turboms24.domain.UserEntity;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ProductRequestDto {
    private UserEntity user;
    private String name;
    private Double price;
    private String priceCurrency;
    private String productDetail;
    private Long carAdId;
    private List<ProductAnalyticsEntity> productAnalytics;


}
