package az.ingress.turboms24.domain;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class ProductDetailEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;
    private String city;
    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private List<ManufactureEntity> manufacturerEntities;
    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private List<ModelEntity> model;
    private Long manufacturerDateTime;
    private String color;
    private String engine;
    private Long kilometers;
    private boolean isNew;
    private Long numOfSeats;
}
