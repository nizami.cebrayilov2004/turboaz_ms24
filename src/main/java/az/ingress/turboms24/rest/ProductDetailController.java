package az.ingress.turboms24.rest;

import az.ingress.turboms24.dto.ProductDetailRequestDto;
import az.ingress.turboms24.dto.ProductDetailResponseDto;
import az.ingress.turboms24.service.ProductDetailService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product-detail")
@RequiredArgsConstructor
public class ProductDetailController {

    private final ProductDetailService productDetailService;

    @GetMapping("/{id}")
    public ProductDetailResponseDto getProductDetailById(@PathVariable Long id) {
        return productDetailService.getProductDetailById(id);
    }

    @PostMapping()
    public ProductDetailResponseDto createProductDetail(@RequestBody @Validated ProductDetailRequestDto productDetailRequestDto) {
        return productDetailService.createProductDetail(productDetailRequestDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProductDetailById(@PathVariable Long id) {
        productDetailService.deleteProductDetailById(id);
    }

    @PutMapping("/{id}")
    public ProductDetailResponseDto update
            (@PathVariable Long id,
             @RequestBody ProductDetailRequestDto productDetailRequestDto) {
        return productDetailService.updateProductDetail(id, productDetailRequestDto);
    }

    @GetMapping
    public Page<ProductDetailResponseDto> getProductDetailList(Pageable pageable) {
        return productDetailService.productDetailList(pageable);
    }


}
