package az.ingress.turboms24.dto;

import az.ingress.turboms24.domain.ProductEntity;
import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@Builder
public class ProductAnalyticsRequestDto {
    private List<ProductEntity> product;
    private Date updateDate;
    private Long views;
}
