package az.ingress.turboms24.service;

import az.ingress.turboms24.dto.ProductRequestDto;
import az.ingress.turboms24.dto.ProductResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductService {
    ProductResponseDto createProduct(ProductRequestDto requestDto);
    ProductResponseDto getProductById(Long id);
    ProductResponseDto updateProduct(Long id, ProductRequestDto productRequestDto);

    void deleteProductById(Long id);

    Page<ProductResponseDto> productList(Pageable page);

}
