package az.ingress.turboms24.service;

import az.ingress.turboms24.domain.UserEntity;
import az.ingress.turboms24.dto.UserRequestDto;
import az.ingress.turboms24.dto.UserResponseDto;
import az.ingress.turboms24.repo.UserRepository;
import lombok.RequiredArgsConstructor;
import org.apache.catalina.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    public UserResponseDto createUser(UserRequestDto userRequestDto) {
        UserEntity userEntity =
                UserEntity.builder()
                        .name(userRequestDto.getName())
                        .products(userRequestDto.getProduct())
                        .phoneNumber(userRequestDto.getPhoneNumber())
                        .build();
        final UserEntity userEntitySaved = userRepository.save(userEntity);
    return UserResponseDto.builder()
            .id(userEntitySaved.getId())
            .phoneNumber(userEntitySaved.getPhoneNumber())
            .name(userEntitySaved.getName())
            .product(userEntitySaved.getProducts())
            .build();
    }
    @Override
    public UserResponseDto getUserById(Long id) {
        final UserEntity user = userRepository.findById(id).orElseThrow();

        return UserResponseDto.builder()
                .id(user.getId())
                .name(user.getName())
                .phoneNumber(user.getPhoneNumber())
                .product(user.getProducts())
                .build();
    }

    @Override
    public UserResponseDto updateUser(Long id, UserRequestDto userRequestDto) {
        UserEntity user =
                UserEntity.builder()
                        .id(id)
                        .phoneNumber(userRequestDto.getPhoneNumber())
                        .name(userRequestDto.getName())
                        .products(userRequestDto.getProduct())
                        .build();
        final UserEntity userEntitySaved = userRepository.save(user);
        return UserResponseDto.builder()
                .id(userEntitySaved.getId())
                .product(userEntitySaved.getProducts())
                .phoneNumber(userRequestDto.getPhoneNumber())
                .name(userEntitySaved.getName())
                .build();
    }

    @Override
    public void deleteUserById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public Page<UserResponseDto> userList(@PageableDefault(size = 7, page = 2) Pageable page) {
        return userRepository.findAll(page)
                .map(userEntity -> UserResponseDto.builder()
                        .id(userEntity.getId())
                        .name(userEntity.getName())
                        .product(userEntity.getProducts())
                        .phoneNumber(userEntity.getPhoneNumber())
                        .build());
    }
}
