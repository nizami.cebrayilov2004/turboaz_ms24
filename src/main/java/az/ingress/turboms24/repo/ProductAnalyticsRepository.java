package az.ingress.turboms24.repo;

import az.ingress.turboms24.domain.ProductAnalyticsEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductAnalyticsRepository extends JpaRepository<ProductAnalyticsEntity, Long> {
}
