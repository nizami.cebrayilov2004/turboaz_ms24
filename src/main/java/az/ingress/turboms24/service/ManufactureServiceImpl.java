package az.ingress.turboms24.service;

import az.ingress.turboms24.domain.ManufactureEntity;
import az.ingress.turboms24.domain.ModelEntity;
import az.ingress.turboms24.dto.ManufactureRequestDto;
import az.ingress.turboms24.dto.ManufactureResponseDto;
import az.ingress.turboms24.repo.ManufactureRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ManufactureServiceImpl implements ManufactureService {
    private final ManufactureRepository manufactureRepository;

    @Override
    public ManufactureResponseDto createManufacture(ManufactureRequestDto manufacturerRequestDto) {
        ManufactureEntity manufactureEntity = ManufactureEntity.builder()
                .name(manufacturerRequestDto.getName())
                .models(manufacturerRequestDto.getModels())
                .build();
        final ManufactureEntity manufactureEntitySaved = manufactureRepository.save(manufactureEntity);
        return ManufactureResponseDto.builder()
                .models((ModelEntity) manufactureEntitySaved.getModels())
                .name(manufactureEntitySaved.getName())
                .build();
    }

    @Override
    public ManufactureResponseDto getManufactureById(Long id) {
        final ManufactureEntity manufacturerEntity = manufactureRepository.findById(id)
                .orElseThrow();
        return ManufactureResponseDto
                .builder()
                .name(manufacturerEntity.getName())
                .models((ModelEntity) manufacturerEntity.getModels())
                .build();
    }

    @Override
    public ManufactureResponseDto updateManufacture(Long id, ManufactureRequestDto manufactureRequestDto) {

        ManufactureEntity manufacturerEntity =
                ManufactureEntity
                        .builder()
                        .id(id)
                        .name(manufactureRequestDto.getName())
                        .models(manufactureRequestDto.getModels())
                        .build();
        final ManufactureEntity manufacturerEntitySaved = manufactureRepository.save(manufacturerEntity);
        return ManufactureResponseDto
                .builder()
                .name(manufacturerEntitySaved.getName())
                .models((ModelEntity) manufacturerEntitySaved.getModels())
                .build();
    }


    @Override
    public void deleteManufactureById(Long id) {
        manufactureRepository.deleteById(id);
    }

    @Override
    public Page<ManufactureResponseDto> manufactureList(@PageableDefault(size = 3, page = 1) Pageable pageable) {
        return manufactureRepository.findAll(pageable)
                .map(manufacturerEntity -> ManufactureResponseDto
                        .builder()
                        .name(manufacturerEntity.getName())
                        .models((ModelEntity) manufacturerEntity.getModels())
                        .build()
                );
    }
}

