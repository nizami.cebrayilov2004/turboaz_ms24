package az.ingress.turboms24.service;

import az.ingress.turboms24.domain.ProductAnalyticsEntity;
import az.ingress.turboms24.dto.ProductAnalyticsRequestDto;
import az.ingress.turboms24.dto.ProductAnalyticsResponseDto;
import az.ingress.turboms24.repo.ProductAnalyticsRepository;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductAnalyticsServiceImpl implements ProductAnalyticsService {
    private final ProductAnalyticsRepository productAnalyticsRepository;


    @Override
    public ProductAnalyticsResponseDto createProductAnalytics(ProductAnalyticsRequestDto productAnalyticsRequestDto) {

        ProductAnalyticsEntity productAnalyticsEntity =
                ProductAnalyticsEntity
                        .builder()
                        .products(productAnalyticsRequestDto.getProduct())
                        .updateDate(productAnalyticsRequestDto.getUpdateDate())
                        .views(productAnalyticsRequestDto.getViews())
                        .build();
        final ProductAnalyticsEntity productDetailEntitySaved = productAnalyticsRepository.save(productAnalyticsEntity);
        return ProductAnalyticsResponseDto
                .builder()
                .id(productDetailEntitySaved.getId())
                .updateDate(productDetailEntitySaved.getUpdateDate())
                .products(productDetailEntitySaved.getProducts())
                .views(productAnalyticsRequestDto.getViews())
                .build();

    }


    @Override
    public ProductAnalyticsResponseDto getProductAnalyticsById(Long id) {
        final ProductAnalyticsEntity productAnalytics = productAnalyticsRepository.findById(id)
                .orElseThrow();
        return ProductAnalyticsResponseDto
                .builder()
                .id(productAnalytics.getId())
                .views(productAnalytics.getViews())
                .products(productAnalytics.getProducts())
                .updateDate(productAnalytics.getUpdateDate())
                .build();
    }


    @Override
    public ProductAnalyticsResponseDto updateProductAnalytics(Long id, ProductAnalyticsRequestDto productAnalyticsRequestDto) {
        ProductAnalyticsEntity productAnalyticsEntity =
                ProductAnalyticsEntity
                        .builder()
                        .id(id)
                        .products(productAnalyticsRequestDto.getProduct())
                        .views(productAnalyticsRequestDto.getViews())
                        .updateDate(productAnalyticsRequestDto.getUpdateDate())
                        .build();
        final ProductAnalyticsEntity productAnalyticsEntitySaved = productAnalyticsRepository.save(productAnalyticsEntity);
        return ProductAnalyticsResponseDto
                .builder()
                .id(productAnalyticsEntitySaved.getId())
                .updateDate(productAnalyticsEntitySaved.getUpdateDate())
                .products(productAnalyticsRequestDto.getProduct())
                .views(productAnalyticsRequestDto.getViews())
                .build();
    }


    @Override
    public void deleteProductAnalyticsById(Long id) {
        productAnalyticsRepository.deleteById(id);
    }

    @Override
    public Page<ProductAnalyticsResponseDto> productAnalyticsList(@PageableDefault(size = 7, page = 1) Pageable pageable) {
        return productAnalyticsRepository.findAll(pageable)
                .map(productAnalyticsEntity -> ProductAnalyticsResponseDto
                        .builder()
                        .id(productAnalyticsEntity.getId())
                        .products(productAnalyticsEntity.getProducts())
                        .views(productAnalyticsEntity.getViews())
                        .updateDate(productAnalyticsEntity.getUpdateDate())
                        .build()
                );
    }
}
