package az.ingress.turboms24.service;

import az.ingress.turboms24.domain.ProductDetailEntity;
import az.ingress.turboms24.dto.ProductDetailRequestDto;
import az.ingress.turboms24.dto.ProductDetailResponseDto;
import az.ingress.turboms24.repo.ProductDetailRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductDetailServiceImpl implements ProductDetailService {

    private final ProductDetailRepository productDetailRepository;


    @Override
    public ProductDetailResponseDto createProductDetail(ProductDetailRequestDto productDetailRequestDto) {

        ProductDetailEntity productDetailEntity =
                ProductDetailEntity
                        .builder()
                        .city(productDetailRequestDto.getCity())
                        .color(productDetailRequestDto.getColor())
                        .isNew(productDetailRequestDto.getIsNew())
                        .model(productDetailRequestDto.getModel())
                        .engine(productDetailRequestDto.getEngine())
                        .kilometers(productDetailRequestDto.getKilometers())
              //          .manufacturer(productDetailRequestDto.getManufacturer())
                        .manufacturerDateTime(productDetailRequestDto.getManufacturerDateTime())
                        .numOfSeats(productDetailRequestDto.getNumOfSeats())
                        .build();
        final ProductDetailEntity productDetailEntitySaved = productDetailRepository.save(productDetailEntity);
        return ProductDetailResponseDto
                .builder()
                .id(productDetailEntitySaved.getId())
                .city(productDetailEntitySaved.getCity())
                .color(productDetailEntitySaved.getColor())
                .isNew(productDetailRequestDto.getIsNew())
//                .model(productDetailEntitySaved.getModel())
                .engine(productDetailEntitySaved.getEngine())
                .kilometers(productDetailEntitySaved.getKilometers())
//                .manufacturer(productDetailEntitySaved.getManufacturer())
                .manufacturerDateTime(productDetailEntitySaved.getManufacturerDateTime())
                .numOfSeats(productDetailEntitySaved.getNumOfSeats())
                .build();

    }

    @Override
    public ProductDetailResponseDto getProductDetailById(Long id) {
        final ProductDetailEntity productDetail = productDetailRepository.findById(id)
                .orElseThrow();
        return ProductDetailResponseDto

                .builder()
                .id(productDetail.getId())
                .city(productDetail.getCity())
                .color(productDetail.getColor())
                .isNew(productDetail.isNew())
                .model(productDetail.getModel())
                .engine(productDetail.getEngine())
                .kilometers(productDetail.getKilometers())
//                .manufacturer(productDetail.getManufacturerEntities())
                .manufacturerDateTime(productDetail.getManufacturerDateTime())
                .numOfSeats(productDetail.getNumOfSeats())
                .build();
    }


    @Override
    public ProductDetailResponseDto updateProductDetail(Long id, ProductDetailRequestDto productDetailRequestDto) {
        ProductDetailEntity productDetailEntity =
                ProductDetailEntity
                        .builder()
                        .Id(id)
                        .city(productDetailRequestDto.getCity())
                        .color(productDetailRequestDto.getColor())
                        .isNew(productDetailRequestDto.getIsNew())
                        .model(productDetailRequestDto.getModel())
                        .engine(productDetailRequestDto.getEngine())
                        .kilometers(productDetailRequestDto.getKilometers())
//                        .manufacturer(productDetailRequestDto.getManufacturer())
                        .manufacturerDateTime(productDetailRequestDto.getManufacturerDateTime())
                        .numOfSeats(productDetailRequestDto.getNumOfSeats())
                        .build();
        final ProductDetailEntity productDetailSaved = productDetailRepository.save(productDetailEntity);
        return ProductDetailResponseDto
                .builder()
                .id(productDetailSaved.getId())
                .city(productDetailSaved.getCity())
                .color(productDetailSaved.getColor())
                .isNew(productDetailSaved.isNew())
                .model(productDetailSaved.getModel())
                .engine(productDetailSaved.getEngine())
                .kilometers(productDetailSaved.getKilometers())
//                .manufacturer(productDetailSaved.getManufacturer())
                .manufacturerDateTime(productDetailSaved.getManufacturerDateTime())
                .numOfSeats(productDetailSaved.getNumOfSeats())
                .build();
    }


    @Override
    public void deleteProductDetailById(Long id) {
        productDetailRepository.deleteById(id);

    }

    @Override
    public Page<ProductDetailResponseDto> productDetailList(@PageableDefault(size = 7, page = 1) Pageable pageable) {
        return productDetailRepository.findAll(pageable)
                .map(productDetailEntity -> ProductDetailResponseDto
                                .builder()
                                .id(productDetailEntity.getId())
                                .city(productDetailEntity.getCity())
                                .color(productDetailEntity.getColor())
                                .isNew(productDetailEntity.isNew())
                                .model(productDetailEntity.getModel())
                                .engine(productDetailEntity.getEngine())
                                .kilometers(productDetailEntity.getKilometers())
//                        .manufacturer(productDetailEntity.getManufacturer())
                                .manufacturerDateTime(productDetailEntity.getManufacturerDateTime())
                                .numOfSeats(productDetailEntity.getNumOfSeats())
                                .build());
    }
}


