package az.ingress.turboms24.rest;
import az.ingress.turboms24.dto.ModelRequestDto;
import az.ingress.turboms24.dto.ModelResponseDto;
import az.ingress.turboms24.service.ModelService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/model")
@RequiredArgsConstructor
public class ModelController {

    private final ModelService modelService;

    @GetMapping("/{id}")
    public ModelResponseDto getModelById(@PathVariable Long id) {
        return modelService.getModelById(id);
    }

    @PostMapping()
    public ModelResponseDto createModel(@RequestBody @Validated ModelRequestDto modelRequestDto) {
        return modelService.createModel(modelRequestDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteModelById(@PathVariable Long id){
        modelService.deleteModelById(id);
    }

    @PutMapping("/{id}")
    public ModelResponseDto update
            (@PathVariable Long id,
             @RequestBody ModelRequestDto modelRequestDto){
        return modelService.updateModel(id, modelRequestDto);
    }

    @GetMapping
    public Page<ModelResponseDto> getModelList(Pageable pageable){
        return modelService.modelList(pageable);
    }




}
