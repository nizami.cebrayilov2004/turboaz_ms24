package az.ingress.turboms24.repo;

import az.ingress.turboms24.domain.ManufactureEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ManufactureRepository extends JpaRepository<ManufactureEntity, Long> {
}
