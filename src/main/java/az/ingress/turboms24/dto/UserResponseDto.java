package az.ingress.turboms24.dto;

import az.ingress.turboms24.domain.ProductEntity;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UserResponseDto {
    private Long id;
    private String name;
    private String phoneNumber;
    private List<ProductEntity> product;
}


