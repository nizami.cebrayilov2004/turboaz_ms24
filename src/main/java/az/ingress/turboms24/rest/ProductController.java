package az.ingress.turboms24.rest;

import az.ingress.turboms24.dto.ProductRequestDto;
import az.ingress.turboms24.dto.ProductResponseDto;
import az.ingress.turboms24.service.ProductService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;

    @GetMapping("/{id}")
    public ProductResponseDto getProductById(@PathVariable Long id){
        return productService.getProductById(id);
    }

    @PostMapping()
    public ProductResponseDto createProduct(@RequestBody @Validated ProductRequestDto productRequestDto){
        return productService.createProduct(productRequestDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProductById(@PathVariable Long id){
        productService.deleteProductById(id);
    }

    @PutMapping("/{id}")
    public ProductResponseDto update(@PathVariable Long id, @RequestBody ProductRequestDto productRequestDto){
        return productService.updateProduct(id, productRequestDto);
    }

    @GetMapping
    private Page<ProductResponseDto> getProductList(Pageable pageable){
        return productService.productList(pageable);
    }
}
