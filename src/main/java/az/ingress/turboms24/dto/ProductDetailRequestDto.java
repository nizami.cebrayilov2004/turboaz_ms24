package az.ingress.turboms24.dto;

import az.ingress.turboms24.domain.ManufactureEntity;
import az.ingress.turboms24.domain.ModelEntity;
import lombok.Builder;
import lombok.Data;

import java.util.List;
@Data
@Builder
public class ProductDetailRequestDto {
    private String city;
    private List<ManufactureEntity> manufacturer;
    private List<ModelEntity> model;
    private Long manufacturerDateTime;
    private String color;
    private String engine;
    private Long kilometers;
    private Boolean isNew;
    private Long numOfSeats;

}
