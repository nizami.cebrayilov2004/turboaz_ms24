package az.ingress.turboms24.dto;

import az.ingress.turboms24.domain.ModelEntity;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ManufactureResponseDto {
    private String name;
    private ModelEntity models;
}

