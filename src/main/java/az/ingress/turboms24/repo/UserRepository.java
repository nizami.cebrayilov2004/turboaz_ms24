package az.ingress.turboms24.repo;

import az.ingress.turboms24.domain.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
}
