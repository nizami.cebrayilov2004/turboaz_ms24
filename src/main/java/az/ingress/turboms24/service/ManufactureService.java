package az.ingress.turboms24.service;

import az.ingress.turboms24.dto.ManufactureRequestDto;
import az.ingress.turboms24.dto.ManufactureResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ManufactureService {


    ManufactureResponseDto createManufacture(ManufactureRequestDto manufacturerRequestDto);

    ManufactureResponseDto getManufactureById(Long id);

    ManufactureResponseDto updateManufacture(Long id, ManufactureRequestDto manufacturerRequestDto);

    void deleteManufactureById(Long id);

    Page<ManufactureResponseDto> manufactureList(Pageable page);
}
