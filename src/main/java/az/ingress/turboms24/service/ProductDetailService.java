package az.ingress.turboms24.service;

import az.ingress.turboms24.dto.ProductDetailRequestDto;
import az.ingress.turboms24.dto.ProductDetailResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductDetailService {


    ProductDetailResponseDto createProductDetail(ProductDetailRequestDto productDetailRequestDto);

    ProductDetailResponseDto getProductDetailById(Long id);

    ProductDetailResponseDto updateProductDetail(Long id, ProductDetailRequestDto productDetailRequestDto);

    void deleteProductDetailById(Long id);

    Page<ProductDetailResponseDto> productDetailList(Pageable page);
}
