package az.ingress.turboms24.rest;
import az.ingress.turboms24.dto.ManufactureRequestDto;
import az.ingress.turboms24.dto.ManufactureResponseDto;
import az.ingress.turboms24.service.ManufactureService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/manufacture")
@RequiredArgsConstructor
public class ManufactureController {

    private final ManufactureService manufactureService;

    @GetMapping("/{id}")
    public ManufactureResponseDto getManufactureById(@PathVariable Long id) {
        return manufactureService.getManufactureById(id);
    }

    @PostMapping()
    public ManufactureResponseDto createManufacture(@RequestBody @Validated ManufactureRequestDto manufactureRequestDto) {
        return manufactureService.createManufacture(manufactureRequestDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteManufactureById(@PathVariable Long id){
        manufactureService.deleteManufactureById(id);
    }

    @PutMapping("/{id}")
    public ManufactureResponseDto update
            (@PathVariable Long id,
             @RequestBody ManufactureRequestDto manufactureRequestDto){
        return manufactureService.updateManufacture(id, manufactureRequestDto);
    }

    @GetMapping
    public Page<ManufactureResponseDto> getManufactureList(Pageable pageable){
        return manufactureService.manufactureList(pageable);
    }




}

